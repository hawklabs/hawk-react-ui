export default function Send(
  hawkClient: HawkClient,
  query: string,
  instance: string,
  queryLanguage: string
): string;
